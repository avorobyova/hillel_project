package com.company;

public class String_1 {
    public String helloName(String name) {
        return ("Hello " + name + '!');
    }

    public String makeAbba(String a, String b) {
        return a + b + b + a;
    }

    public String makeTags(String tag, String word) {
        return ("<" + tag + ">" + word + "</" + tag + ">");
    }

    public String makeOutWord(String out, String word) {
        return out.substring(0, 2) + word + out.substring(2, 4);
    }

    public String extraEnd(String str) {
        String s = str.substring(str.length() - 2, str.length());
        return s + s + s;

    }

    public String firstTwo(String str) {
        if (str.length() < 3) {
            return str;
        } else return str.substring(0, 2);
    }

    public String firstHalf(String str) {
        // if (str.length()%2==0){
        return str.substring(0, str.length() / 2);
//  }
        // else return str;
    }

    public String withoutEnd(String str) {
        return str.substring(1, str.length() - 1);
    }

    public String comboString(String a, String b) {
        if (a.length() > b.length()) {
            return b + a + b;
        } else return a + b + a;
    }

    public String nonStart(String a, String b) {
        return a.substring(1, a.length()) + b.substring(1, b.length());
    }

    public String left2(String str) {
        return str.substring(2, str.length()) + str.substring(0, 2);
    }

    public String right2(String str) {
        return str.substring(str.length() - 2, str.length()) + str.substring(0, str.length() - 2);
    }
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


    public String theEnd(String str, boolean front) {
        if (front == true) {
            return str.substring(0, 1);
        } else return str.substring(str.length() - 1);
    }


    public String withouEnd2(String str) {
        if (str.length() < 3) {
            return "";
        } else return str.substring(1, str.length() - 1);

    }


    public String middleTwo(String str) {
        if (str.length() % 2 == 0) {
            return str.substring(str.length() / 2 - 1, str.length() / 2 + 1);
        } else return str = "";
    }

    public boolean endsLy(String str) {
        if (str.length() > 1 && str.substring(str.length() - 2, str.length()).equals("ly")) {
            return true;
        } else return false;
    }

    public String nTwice(String str, int n) {
        return (str.substring(0, n) + str.substring(str.length() - n));
    }

    public String twoChar(String str, int index) {
        if (str.length() > 2 && index < str.length() - 1 && index > 0) {
            return str.substring(index, index + 2);
        } else return str.substring(0, 2);
    }

    public String middleThree(String str) {
        if (str.length() > 3) {
            int a = (str.length() - 1) / 2;
            return str.substring(a - 1, a + 2);
        } else return str;
    }

//    public boolean hasBad(String str) {
//        if (str.substring(0,3).equals("bad") || str.substring(1,4).equals("bad") ){
//            return true;
//        }
//        else return false;
//    }


    public boolean hasBad(String str) {
        if (str.length() > 2) {
            if (str.substring(0, 3).equals("bad") || (str.length() > 3 && str.substring(1, 4).equals("bad")))
                return true;
            else return false;
        } else return false;
    }


    public String atFirst(String str) {
        if (str.length() > 1) return str.substring(0, 2);
        if (str.length() == 1) return str + "@";
        else return "@@";
    }

    public String lastChars(String a, String b) {
        if (a.length()>0 && b.length()>0)
        {
            return a.substring(0,1)+b.substring(b.length()-1);
        }
        if  (a.length()>0 && b.length()==0) return a.charAt(0)+"@";
        if (a.length()==0 && b.length()>0) return "@"+b.charAt(b.length()-1);
        else return "@@";
    }

//    public String conCat(String a, String b) {
//        // if (a.length() <1|| b.length()<1) return a+b;
//        if (!(a.substring(a.length()-1).equals(b.substring(0,1)))) return a+b;
//        else return a+b.substring(1);
//    }????????????

    public String conCat(String a, String b) {
        if ((a.length() <1 &&b.length()>1)||(b.length() <1 &&a.length()>1))return a+b;

        if (a.substring(a.length()-1).equals(b.substring(0,1))) return a+b.substring(1);
        else return a+b;
    }

    public String lastTwo(String str) {
        if (str.length() >1) return str.substring(0,str.length()-2)+str.charAt(str.length()-1)+str.charAt(str.length()-2);
        else return str;
    }

    public String seeColor(String str) {

        if (str.length() >2 && str.substring(0,3).equals("red") )return str.substring(0,3);
        if (str.length() >3 &&str.substring(0,4).equals("blue")) return str.substring(0,4);
        else return "";

    }





}
