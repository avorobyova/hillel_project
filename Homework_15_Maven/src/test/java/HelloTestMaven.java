import org.junit.Test;

import static org.junit.Assert.*;

public class HelloTestMaven {

    @Test
    public void testHello() {
        HelloWorld hello = new HelloWorld();
        assertEquals("Hello, world!", hello.sayHello());
    }
}
