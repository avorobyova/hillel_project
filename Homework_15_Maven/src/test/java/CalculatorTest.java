import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.*;

/*1.Создать по 6 тестов для проектов мавен и градл. 3 теста позитивных и 3 негативных.
    Использовать такие Junit  Assert: Assert.assertFalse, Assert.AssertEquals, assertThat, assertTrue
     */
public class CalculatorTest {

    @Test(timeout = 50)
    public void addTest() throws Exception {
        Calculator calculator = new Calculator();
        int add = calculator.add(3, 7);
        assertEquals(10, add);
    }

    @Test(timeout = 50)
    public void addFailedTest() throws Exception {
        Calculator calculator = new Calculator();
        int add = calculator.add(3, 7);
        assertEquals(9, add);

    }

    @Test(timeout = 20)
    public void divTest() throws Exception {
        Calculator calculator = new Calculator();
        double div = calculator.div(5, 2);
        assertNotEquals(4, div);
    }

    @Test(timeout = 20)
    public void divFailedTest() throws Exception {
        Calculator calculator = new Calculator();
        double div = calculator.div(5, 2);
        assertNotEquals(2.5, div);
    }

    @Test
    public void evenNumberTest() {
        Calculator calculator = new Calculator();
        assertFalse(calculator.isEvenNumber(3));
    }

    @Test
    public void evenNumberFailedTest() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.isEvenNumber(3));
    }

    @Test
    public void multilplyTest() throws Exception {
        Calculator calculator = new Calculator();
        double result = calculator.multiply(3, 2.5);
        assertThat(result, CoreMatchers.is(7.5));
    }

}
