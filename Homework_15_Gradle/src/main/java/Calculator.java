public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }

    public double div(double a, double b) {
        return a / b;
    }

    public boolean isEvenNumber(int number) {
        boolean result = false;
        if (number % 2 == 0) {
            result = true;
        }
        return result;
    }
}
