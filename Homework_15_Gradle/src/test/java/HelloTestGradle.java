import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HelloTestGradle {

    @Test
    public void testHello() {
        HelloWorld hello = new HelloWorld();
        assertEquals("Hello, world!", hello.sayHello());
    }
}
