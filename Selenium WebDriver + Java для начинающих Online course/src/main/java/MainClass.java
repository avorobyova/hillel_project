import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainClass {

    static WebDriver driver;
    static WebDriverWait wait;
    static WebDriverWait wait2;


    public static void main(String[] args) throws IOException {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver");
        driver = new ChromeDriver();

        //create format for naming file screenshot
        Date dateNow = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh_mm_ss");
        String fileName = format.format(dateNow) +".png";

        driver.navigate().to("https://www.google.com/");
        driver.manage().window().maximize();
        String mainWindow = driver.getWindowHandle();

        driver.get("https://www.wikipedia.org/");
        //driver.manage().window().setSize(new Dimension(900,500));
        driver.navigate().back(); //возвращаемся на предыдущую страницу

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.navigate().refresh(); // перезагрузка страницы
        driver.navigate().forward(); //на страницу вперед

        /*неявное ожидание используется для того чтобы задать ожидание для каждого поиска элемента - т.е. каждый раз
        когда мы ищем элемент мы будет выжидать данное время
         */
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);


        //явное ожидание которое используется один раз для конкреного условия
        wait = new WebDriverWait(driver, 5);
        wait2 = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"searchInput\"]")));


        //WebElement link = driver.findElement(By.linkText("Log in"));
//        WebElement searchField = driver.findElement(By.name("search"));
//        WebElement searchButton = driver.findElement(By.className("searchButton"));
        driver.findElement(By.xpath("//*[@id=\"searchInput\"]")).sendKeys(Keys.chord(Keys.SHIFT,"Selenium Webdriver" ));
        driver.findElement(By.xpath("//*[@id=\"search-form\"]/fieldset/button")).click();

        String select = Keys.chord(Keys.COMMAND, "a"); //select all text - сочетание клавиш
        String cut = Keys.chord(Keys.COMMAND, "x"); // cut the text
        String past = Keys.chord(Keys.COMMAND, "v"); // past the text


        System.out.println(driver.findElement(By.xpath("//*[@id=\"ooui-php-1\"]")).getAttribute("value"));
        driver.findElement(By.xpath("//*[@id=\"ooui-php-1\"]")).clear();
        WebElement link2 = driver.findElement(By.partialLinkText("Selenium"));
        WebElement link = driver.findElement(By.xpath("//*[@id=\"footer-places-about\"]/a"));
        System.out.println(link.getText());
        link.click();


        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());

        //open additional tab in browser
        // driver.findElement(By.cssSelector("body")).sendKeys(Keys.COMMAND +"t");
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get("https://courses.edx.org/register");

        selectOption("register-country", 5);
        selectOption("register-country", 25);


        driver.findElement(By.xpath("//*[@id=\"register-country\"]")).click(); //click on drop-down menu
        driver.findElement(By.xpath("//*[@id=\"register-country\"]/option[15]")).click(); //select option in drop-down menu
        //driver.switchTo().window(tabs.get(0));
        //driver.close();
        driver.get("https://rozetka.com.ua/mobile-phones/c80003/preset=smartfon/");

        //list of web elements
        List<WebElement> checkboxes = driver.findElements(By.xpath("//aside[@class='sidebar']//div[6]//ctg-filter-checkbox/ul[1]/li[@class='checkbox-filter__item']"));
        checkboxes.get(3).click();

        if (checkboxes.size() == 7) System.out.println("It's okay");
        else {
            System.out.println("Fail!");
        }

       /*to select all checkboxes need to use for each loop
        for (WebElement checkbox: checkboxes){
            checkbox.click();
        }
        */
        driver.findElement(By.xpath("//div[1]/app-rz-header/header/div/div[2]/div[1]/button/span")).click();
        WebElement linkOnNotebook = driver.findElement(By.xpath("//div[1]/app-rz-header//div[1]/fat-menu/div/ul/li[1]/a"));
        Actions actions = new Actions(driver);
        actions.moveToElement(linkOnNotebook).build().perform();

//        actions.dragAndDropBy(linkOnNotebook,9,30).build().perform();
//        actions.clickAndHold(linkOnNotebook).moveToElement(link).release().build().perform();
//        Action action = actions.clickAndHold(linkOnNotebook).moveToElement(link).release().build();
//
//        action.perform();
//        actions.doubleClick();


        driver.get("https://www.w3schools.com/html/html_tables.asp");
        WebElement tableElement = driver.findElement(By.xpath("//table[@id =\"customers\"]"));

        Table table = new Table(tableElement, driver);

        System.out.println("Rows number is " + table.getRows().size());
        System.out.println(table.getValueFromCell(2, 3));
        System.out.println(table.getValueFromCell(4, 1));
        System.out.println(table.getValueFromCell(4, "Company"));


        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,1000)", "");
        jse.executeScript("alert('HELLO WORLD!');");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.switchTo().alert().dismiss();
        //driver.switchTo().alert().accept();

        driver.switchTo().window(mainWindow);

        //driver.quit();
//
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileHandler.copy(screenshot,new File("src/main//"+fileName));

    }

    public static void selectOption(String listName, int option) {
        String listXPath = String.format("//*[@id=\"%s\"]", listName);
        String optionXPath = String.format("//*[@id=\"register-country\"]/option[%s]", option);
        driver.findElement(By.xpath(listXPath)).click(); //click on drop-down menu
        wait2.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(optionXPath))));
        driver.findElement(By.xpath(optionXPath)).click(); //select option in drop-down menu
        //wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(optionXPath)));
    }
}
