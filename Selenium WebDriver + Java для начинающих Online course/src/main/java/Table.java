import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* work with tables
 */
public class Table {
    private WebElement tableElement;

    private WebDriver driver;

    //constructor for that class
    public Table(WebElement tableElement, WebDriver driver) {
        this.tableElement = tableElement;
        this.driver = driver;
    }

    //method that return list of rows that table
    public List<WebElement> getRows() {
        List<WebElement> rows = tableElement.findElements(By.xpath(".//tr"));
        rows.remove(0);
        return rows;
    }

    //method that return list of headers of first row
    public List<WebElement> getHeaders() {
        WebElement headersRow = tableElement.findElement(By.xpath(".//tr[1]"));
        List<WebElement> headersColumns = headersRow.findElements(By.xpath(".//th"));
        return headersColumns;
    }

    // split all rows on columns - мы получаем список строк которые будут разбиты на столбцы
    public List<List<WebElement>> getRowsWithColumns() { //get list of list columns
        List<WebElement> rows = getRows();
        List<List<WebElement>> rowsWithColumns = new ArrayList<List<WebElement>>();
        for (WebElement row : rows) {
            List<WebElement> rowWithColumns = row.findElements(By.xpath(".//td"));
            rowsWithColumns.add(rowWithColumns);
        }
        return rowsWithColumns;
    }

    public String getValueFromCell(int rowNumber, int columnNumber) {
        List<List<WebElement>> rowsWithColumns = getRowsWithColumns();
        WebElement cell = rowsWithColumns.get(rowNumber - 1).get(columnNumber - 1);
        return cell.getText();

    }

    public List<Map<String, WebElement>> getRowsWithColumnsByHeader() {
        List<List<WebElement>> rowsWithColumns = getRowsWithColumns();
        List<Map<String, WebElement>> rowsWithColumnsByHeader = new ArrayList<Map<String, WebElement>>();
        Map<String, WebElement> rowByHeaders;
        List<WebElement> headersColumns = getHeaders();

        for (List<WebElement> row : rowsWithColumns) {
            rowByHeaders = new HashMap<String, WebElement>();
            for (int i = 0; i < headersColumns.size(); i++) {
                String headers =headersColumns.get(i).getText();
                WebElement cell = row.get(i);
                rowByHeaders.put(headers,cell);

            }
            rowsWithColumnsByHeader.add(rowByHeaders);
        }
        return rowsWithColumnsByHeader;
    }

    public String getValueFromCell(int rowNumber, String columnName) {
        List<Map<String, WebElement>> rowsWithColumnsByHeader = getRowsWithColumnsByHeader();
        return rowsWithColumnsByHeader.get(rowNumber -1).get(columnName).getText();
    }


}
