package com.company;

public class Main {

    public static void main(String[] args) {
        ElectronicDevice iPhone7 = new ElectronicDevice();
        iPhone7.setYear(2016);
        iPhone7.setColor("jet black");
        iPhone7.setCost(300);
        iPhone7.setBrandName("Apple");
        iPhone7.setCapacity(6000);


        ElectronicDevice iPhoneXS = new ElectronicDevice("Apple", "IPhoneXS", "black", 300, 9.8, 64, 7000, 2018);
        ElectronicDevice samsungS9 = new ElectronicDevice("Samsung", "GalaxyS9");
        ElectronicDevice samsungA5 = new ElectronicDevice("Samsung", "A5", "white");
        ElectronicDevice meizu = new ElectronicDevice("Meizu", "Z9", 200.7, 6.8);
        ElectronicDevice iPhone11 = new ElectronicDevice("Apple", "IPhone 11 Pro","green",1100 );
        ElectronicDevice iPhoneXR = new ElectronicDevice("IPhone XR", "red", 800.5, 128, 2018);


        System.out.println("There're following Apple devices:");
        System.out.println(iPhone7.toString() + iPhone11.toString() + iPhoneXR.toString() + iPhoneXS.toString());
        System.out.println("The most popular phone device in 2019 year is " +iPhone11.getModel() + " designed by "+ iPhoneXS.getBrandName());
        System.out.println(meizu.getBrandName() + meizu.getModel() + " much cheaper than "+ iPhoneXR.getModel() + " and "+iPhoneXS.getModel());


        Calculator math = new Calculator();

        System.out.println("The sum of two numbers is " + math.adding(6, 8));
        System.out.println("The difference of two numbers " + math.minus(10.8, 9));
        System.out.println("The divide is " + math.divide(27, 9));
        System.out.println("The sum of three numbers " + math.adding(6, 7.8, 3.9));
        System.out.println("The multiplication of three numbers " + math.multiplication(7.3, 2.5, 7.1));
        System.out.println("The multiplication of two numbers " + math.multiplication(3, 3.3));


        math.squared(8);

        math.cube(3);


        math.squareRoot(25);


    }
//
}
