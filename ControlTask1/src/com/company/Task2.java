package com.company;

public class Task2 {

    /*
    У вас есть int arr[]={4,9,15,7}; Необходимо распечатать содержимое array c помощью do/while и for, for each
     */

    public static void main(String[] args) {
        int arr[] = {4, 9, 15, 7};

        System.out.println("do while loop");
        int i = 0;
        do {
            System.out.print(arr[i] + ", ");
            i++;
        } while (i < arr.length);
        System.out.println("\n");

        System.out.println("for loop");
        int j;
        for (j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + ", ");
        }
        System.out.println("\n");

        System.out.println("for each loop");
        for (int x : arr) {
            System.out.print(x);
            System.out.print(", ");
        }
    }
}
