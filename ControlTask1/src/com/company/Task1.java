package com.company;

import java.util.Scanner;

public class Task1 {

    /*
    Вы вводите число 1, 2 или 3, а программа должна сказать, какое число ввёл пользователь: 1, 2, или 3.
    (Пример вывода: Вы ввели число 1) - сделать двумя способами через if and switch.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите число:");

        if (sc.hasNextInt()) {
            int number = sc.nextInt();
            printNumberViaIf(number);
            printNumberViaSwitch(number);
        } else {
            System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
        }

    }


    public static void printNumberViaIf(int n) {
        if (n == 1) System.out.println("Спасибо! Вы ввели число 1");
        else if (n == 2) System.out.println("Спасибо! Вы ввели число 2");
        else if (n == 3) System.out.println("Спасибо! Вы ввели число 3");
    }

    public static void printNumberViaSwitch(int n) {

        switch (n) {
            case (1):
                System.out.println("Спасибо! Вы ввели число 1");
                break;
            case (2):
                System.out.println("Спасибо! Вы ввели число 2");
                break;
            case (3):
                System.out.println("Спасибо! Вы ввели число 3");
                break;

        }

    }
}
