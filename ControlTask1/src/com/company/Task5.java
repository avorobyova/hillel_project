package com.company;

public class Task5 {

    /*
    Создать array  размер которого 4. Добавить в него 4 любых фрукта.
    Вывести название фруктов через for each, а также вычислить размер массива и вывести его тоже.
    После этого вывести фрукты в обратном порядке через do/while.
     */


    public static void main(String[] args) {
        String arr[] = {"apple", "banana", "blackberry", "blueberry"};

        //for each loop
        System.out.println("Массив: ");
        for (String x : arr) {
            System.out.print(x + ", ");
        }
        System.out.println();

        System.out.println(arr.length + " - количество элементов в массиве.");

        System.out.println();

        //do_while loop
        int i = arr.length - 1;
        System.out.println("Массив в обратном порядке: ");
        do {
            System.out.print(arr[i] + ", ");
            i--;
        } while (i >= 0);
    }
}
