package com.company;

import java.util.Scanner;

public class Task3 {
    /*
    Если вводим число 2. Программа вычислит сумму 0 + 2 и выдаст ответ 2. Если вводим число 4.
    Программа должна посчитать сумму чисел от 2 до 4, то есть 2+3+4 и выдать ответ 9.
     Ваша программа должна суммировать числа от 2 и до указанного. Можно использовать Scanner sc = new Scanner(System.in);
     */

    public static void main(String[] args)  {
        Scanner scr = new Scanner(System.in);
        System.out.println("Введите число:");

        if (scr.hasNextInt()) {
            int n = scr.nextInt();
            int result = 0;
            for (int x = 2; x < n+1; x++) {
                result += x;
            }
            System.out.println("Спасибо! Результат вичесления: " + result);

        } else {
            System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
        }


    }
}
