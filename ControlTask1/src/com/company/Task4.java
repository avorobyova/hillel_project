package com.company;

import java.util.Scanner;

public class Task4 {
    /*
    Если вы достигли 18 лет,вы можете голосовать,если нет, то не можете.Сделать через тернарный оператор
     */

    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);
        System.out.println("Введите Ваш возраст:");
        if (scr.hasNextInt()) {
            System.out.println( scr.nextInt() >= 18 ? "Вы можете голосовать":"Вы не можете голосовать");

        } else {
            System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
        }
    }
}
