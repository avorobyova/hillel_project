package com.company;

public class Task6 {

    /*
    int[][] arr = { {1, 7, 9, 10}, {5, 2, 2} };
    У вас есть двумерный массив, необходимо вывести его значения в обратном порядке (2,2,5,10,9,7,1)
     */

    public static void main(String[] args) {
        int[][] arr = {{1, 7, 9, 10}, {5, 2, 2}};
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = arr[i].length - 1; j >= 0; j--) {
                System.out.print(" " + arr[i][j] + " ");
            }
            System.out.print("");
        }
    }
}
