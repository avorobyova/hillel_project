import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ListOfLocators {
    WebDriver driver;
    /* 1. logo
     */
    WebElement logoCss = driver.findElement(By.cssSelector("header.page-header.page-header--main.page-header--transparent > div > a"));
    WebElement logoXpath = driver.findElement(By.xpath("//header[1]/div/a"));
    /*2. "Записаться на курс" button
     */
    WebElement buttonCss = driver.findElement(By.cssSelector("#signCoursesButton"));
    WebElement buttonXpath = driver.findElement(By.xpath("//*[@id=\"signCoursesButton\"]"));
    /*3. "Карта Украины" image
     */
    WebElement mapCss = driver.findElement(By.cssSelector("section > div > img"));
    WebElement mapXpath = driver.findElement(By.xpath("//section/div/img"));
    /*
    4. "Самый крупный в стране YouTube-канал" Youtube logo image
     */
    WebElement youtubeChanelCss = driver.findElement(By.cssSelector("a > span > svg > use"));
    WebElement youtubeChanelXpath = driver.findElement(By.xpath("//a/span/svg/use"));
    /*
     5. "Курсы" section in header of web-site
     */
    WebElement courseCss = driver.findElement(By.cssSelector("#course"));
    WebElement courseXpath = driver.findElement(By.xpath("//*[@id=\"course\"]"));
    /*
    6. "Тестирование" section in "Курсы" section
     */
    WebElement testingCss = driver.findElement(By.cssSelector("li.menu__item.menu__item--active > a"));
    WebElement testingXpath = driver.findElement(By.xpath("//*[@id=\"courseMenuNav\"]/div/div[1]/ul/li[2]/a"));
    /*
     7. QA Automation course in "Тестирование" section
     */
    WebElement qaAutomationCss = driver.findElement(By.cssSelector("div.menu__courses.active > section > a:nth-child(2) > div"));
    WebElement qaAutomationXpath = driver.findElement(By.xpath("//div[13]/section/a[2]/div"));
    /*
     8. "Спасибо за интерес к нашим курсам" modal window
     */
    WebElement modalWindowCss = driver.findElement(By.cssSelector("#magnet > div > div"));
    WebElement modalWindowXpath = driver.findElement(By.xpath("//*[@id=\"magnet\"]/div/div"));
    /*
     9. "Введите ваше имя" input field
     */
    WebElement enterYourNameInputFieldCss = driver.findElement(By.cssSelector("#inputMagnetName"));
    WebElement enterYourNameInputFieldXpath = driver.findElement(By.xpath("//*[@id=\"inputMagnetName\"]"));
    /*
    10. "email" input field
    */
    WebElement enterEmailFieldCss = driver.findElement(By.cssSelector("#inputMagnetEmail"));
    WebElement enterEmailFieldXpath = driver.findElement(By.xpath("//*[@id=\"inputMagnetEmail\"]"));
    /*
    11. footer of "Спасибо за интерес к нашим курсам" modal window
    */
    WebElement footerCss = driver.findElement(By.cssSelector("#magnet > div > div > footer"));
    WebElement footerXpath = driver.findElement(By.xpath("//*[@id=\"magnet\"]/div/div/footer"));
    /*
    12. "Отправьте нам сообщение" modal window of Feedback
    */
    WebElement modalWindowFeedbackCss = driver.findElement(By.cssSelector("#jcont"));
    WebElement modalWindowFeedbackXpath = driver.findElement(By.xpath("//*[@id=\"jcont\"]"));
    /*
    13. "Ваше сообщение" input field
    */
    WebElement yourMessageInputFieldCss = driver.findElement(By.cssSelector("jdiv > jdiv.tdTextarea_3O > textarea"));
    WebElement yourMessageInputFieldXpath = driver.findElement(By.xpath("//jdiv/jdiv[1]/textarea"));
    /*
    14. "Ваше имя" input field
    */
    WebElement yourNameInputFieldCss = driver.findElement(By.cssSelector("jdiv > jdiv:nth-child(1) > input"));
    WebElement yourNameInputFieldXpath = driver.findElement(By.xpath("//jdiv/jdiv[1]/input"));
    /*
    15. "Ваш телефон" input field
    */
    WebElement yourPhoneInputFieldCss = driver.findElement(By.cssSelector("jdiv.box_3u > jdiv > jdiv > jdiv > jdiv > jdiv:nth-child(3) > input"));
    WebElement yourPhoneInputFieldXpath = driver.findElement(By.xpath("//jdiv[4]/jdiv/jdiv/jdiv/jdiv/jdiv[3]/input"));
    /*
   16. "Ваш e-mail" input field
   */
    WebElement yourEmailInputFieldCss = driver.findElement(By.cssSelector("jdiv.box_3u > jdiv > jdiv > jdiv > jdiv > jdiv:nth-child(2) > input"));
    WebElement yourEmailInputFieldXpath = driver.findElement(By.xpath("//jdiv[4]/jdiv/jdiv/jdiv/jdiv/jdiv[2]/input"));
    /*
   17. "Отправить" button of "Отправьте нам сообщение" modal window of Feedback
   */
    WebElement sendButtonCss = driver.findElement(By.cssSelector("jdiv.box_3u > jdiv > jdiv > jdiv > jdiv > jdiv:nth-child(4) > jdiv"));
    WebElement sendButtonXpath = driver.findElement(By.xpath("//jdiv[4]/jdiv/jdiv/jdiv/jdiv/jdiv[4]/jdiv"));


}
