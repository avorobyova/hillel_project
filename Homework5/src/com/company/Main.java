package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        getNumber();
        charLoop();
        Grade();
        ATB();
    }


    public static void getNumber() {
        // for loop
        for (int a = 3; a < 70; a = a + 6) {
            System.out.println(a);
        }

        // do while loop
        int x = 3;
        do {
            System.out.println(x);
            x = x + 6;
        } while (x < 70);
    }

    public static void charLoop() {
        for (String n = "*"; n.length() < 5; n = n + "*") {
            System.out.println(n);
        }
    }

    public static void Grade() {

//        Ниже 25 - F b. 25 to 45 - E c. 45 to 50 - D d. 50 to 60 - C e. 60 to 80 - B f. Выше 80 - A

        Scanner scr = new Scanner(System.in);
        System.out.println("Введите Ваша оценку:");
        int numberFromSun = scr.nextInt();
        if (numberFromSun >= 80 && numberFromSun <= 100) System.out.println("A");
        else if (numberFromSun >= 60) System.out.println("B");
        else if (numberFromSun >= 50) System.out.println("C");
        else if (numberFromSun >= 45) System.out.println("D");
        else if (numberFromSun >= 25) System.out.println("E");
        else if (numberFromSun > 0) System.out.println("F");
        else System.out.println("Вы вели неверное значение!");

    }

    public static void ATB() {
        Scanner scr = new Scanner(System.in);
        System.out.println("Введите стоимость вашей покупки:");
        int Amount = scr.nextInt();

        switch (Amount) {
            case (202):
                System.out.println("не хватает 2 grn");
                break;
            case (200):
                System.out.println("счастливчик!");
                break;
            case (100):
                System.out.println("пойду возьму что-то еще");
                break;
            default:
                System.out.println("что-то пошло не так");
                break;


        }


    }


}