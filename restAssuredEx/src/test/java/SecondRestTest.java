import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class SecondRestTest {
    private static RequestSpecification requestSpec;

    @BeforeClass
    public static void createRequestSpecification(){
        requestSpec = new RequestSpecBuilder().
                setBaseUri("http://api.zippopotam.us").
                //addHeader("jhjhk","wefeffwe").
                build();
    }

    @Test
    public void expectBeverlyHills_withRequestSpec(){
        given().
                spec(requestSpec).
                when().
                get("us/90210").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void expectSchenectady_withRequestSpec(){
        given().
                spec(requestSpec).
                when().
                get("us/12345").
                then().
                assertThat().
                statusCode(200);
    }

    private static ResponseSpecification responseSpec;

    @AfterClass
    public static void createResponseSpecification(){
        responseSpec = new ResponseSpecBuilder().
                expectStatusCode(200).expectContentType(ContentType.JSON).
                //addHeader("jhjhk","wefeffwe").
        build();
    }
}
