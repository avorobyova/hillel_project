import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class ThirdTest {

    @Test
    public void expectBeverlyHills(){
        Location location = given().
                when().log().everything().get("http://api.zippopotam.us/us/90210").as(Location.class);
        Assert.assertEquals("Beverly Hills",
                location.getPlaces().get(0).getPlaceName());
    }
    @Test
    public void checkStatusCode_expect200() {

        Location location = new Location();
        location.setCountry("Netherlands");

        given().
                contentType(ContentType.JSON).
                body(location).
                log().body().
                when().
                post("http://api.zippopotam.us/lv/1050").
                then().
                assertThat().
                statusCode(405);
    }
}
