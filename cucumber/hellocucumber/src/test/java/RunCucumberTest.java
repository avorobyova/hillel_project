import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty","html:target/cucumber-reports"},
        tags = "@cucumber",
        glue = {"steps"},
        features = "src/test/resources/features")
public class RunCucumberTest {

}
