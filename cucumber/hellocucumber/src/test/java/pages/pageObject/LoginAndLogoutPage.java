package pages.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import utils.Waiters;

public class LoginAndLogoutPage extends BasePage {

    public static final String URL_AUTOPRACTICE_LOGIN = "http://automationpractice.com/index.php?controller=authentication&back=my-account";

    public LoginAndLogoutPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div/nav/div[1]/a")
    private WebElement clickBySignIn;
    //"//a[@class='login']"

    @FindBy(xpath = "//a[@class='logout']")
    private WebElement clickByLogout;

    public void openLoginPage() {
        clickBySignIn.click();
        Waiters.waitForUrl(driver, Waiters.TIME_FIVE, URL_AUTOPRACTICE_LOGIN);
    }

//    public void loginByCredentials() {
//        driver.findElement(By.xpath("//input[@id='email']")).sendKeys(email);
//        driver.findElement(By.xpath("//input[@id='passwd']")).sendKeys(password);
//        driver.findElement(By.xpath("//p[@class='submit']//span[1]")).click();
//    }

    public void clickLogOut(){
        clickByLogout.click();
    }
}
