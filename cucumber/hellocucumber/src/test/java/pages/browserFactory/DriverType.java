package pages.browserFactory;

public enum  DriverType {
    CHROME,
    FIREFOX,
    SAFARI;
}
