package com.company;

import java.util.*;

public class CollectionsHomework {
    public List<Integer> list;

    //конструктор
    public CollectionsHomework(int amountOfItems) {
        this.list = getListOfRandomIntegers(amountOfItems);
    }

    //создание коллекции целых чисел, используя класс Random().
    public LinkedList<Integer> getListOfRandomIntegers(int amountOfItems) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i : new Random().ints(amountOfItems, -5, 10).toArray()) {
            list.add(i);
        }
        return list;
    }

    @Override
    public String toString() {
        return list.toString();
    }

    //создание новой коллекцию, переписав в нее элементы из первой коллекции
    public LinkedList<Integer> copyTheCollection() {
        LinkedList<Integer> copy = new LinkedList<>();
        copy.addAll(list);
        return copy;
    }

    //определение уникальных чисел
    public Set<Integer> getUniqueNumbers() {
        HashSet<Integer> uniqueNumbers = new HashSet<>();
        uniqueNumbers.addAll(list);
        return uniqueNumbers;
    }

    //определить количество каждого уникального числа
    public Map<Integer, Integer> getOccurrences() {
        Map<Integer, Integer> occurrences = new HashMap<>();

        for (int num : list) occurrences.put(num, occurrences.containsKey(num) ? occurrences.get(num) + 1 : 1);

        return occurrences;
    }

    //отсортировать коллекцию
    public LinkedList<Integer> sortCollection() {
        LinkedList<Integer> sorted = copyTheCollection();
        Collections.sort(sorted);
        return sorted;
    }

    //найти макс, мин и сумму чисел коллекции
    public int findMinValueInCollection() {
        List<Integer> minimum = copyTheCollection();
        return Collections.min(minimum);
    }

    public int findMaxValueInCollection() {
        LinkedList<Integer> maximum = copyTheCollection();
        return Collections.max(maximum);
    }

    public int sumCollection() {
        int sum = 0;
        for (int i : copyTheCollection()) sum += i;
        return sum;
    }

    //создание коллекции, содержащей все положительные числа первой коллекции
    public LinkedList<Integer> getPositiveNumbers() {
        LinkedList<Integer> positive = new LinkedList<>();
        for (int i : list) if (i >= 0) positive.add(i);
        return positive;
    }

    //метод, который удаляет из первой коллекции все нечетные числа и возвращает новый массив
    public LinkedList<Integer> deleteOddNumbers() {
        LinkedList<Integer> copy = new LinkedList<>();
        int id = 0;
        for (int i : list) {
            if (i % 2 == 0) {
                copy.add(i);
                id++;
            }
        }
        return copy;
    }

    //найти указанное число в массиве (например 7). В случае, если указанного числа нет- выводите сообщение об ошибке.
    public int searchNumber(int theNumber) throws Exception {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == theNumber) {
                return theNumber;
            }
        }
        throw new Exception("There's no such element in list like " + theNumber);
    }
}

