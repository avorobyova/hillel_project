package com.company;

public class Main {

    public static void main(String[] args) {
        CollectionsHomework hw = new CollectionsHomework(15);
        System.out.println("The collection: " + hw.toString());
        System.out.println("Copy of the collection: " + hw.copyTheCollection());
        System.out.println("Unique numbers: " + hw.getUniqueNumbers());
        System.out.println("List of occurrences: " + hw.getOccurrences());
        System.out.println("Sorted: " + hw.sortCollection());
        System.out.println("Minimum value in an collection: " + hw.findMinValueInCollection());
        System.out.println("Maximum value in an collection: " + hw.findMaxValueInCollection());
        System.out.println("Sum of items in the collection: " + hw.sumCollection());
        System.out.println("All positive numbers in the collection: " + hw.getPositiveNumbers());
        System.out.println("Collection without odd numbers: " + hw.deleteOddNumbers());
        try {
            System.out.println("If there's number 7 in collection, it will be here... : " + hw.searchNumber(7));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
