package firstSuites.firstTest;

import firstSuites.BaseTest;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.pageObject.GoogleResultPage;
import pages.pageObject.GoogleSearchPage;

import java.io.File;
import java.io.IOException;

import static pages.pageObject.GoogleSearchPage.URL_AUTOPRACTICE;


public class MyFirstSeleniumTest extends BaseTest {


    GoogleSearchPage googleSearchPage;
    GoogleResultPage googleResultPage;
    WebDriver driver;

    @BeforeTest
    public void startDriver() {
        driver = getDriver();
        googleSearchPage = new GoogleSearchPage(driver);
        googleResultPage = new GoogleResultPage(driver);
    }

    @Severity(SeverityLevel.CRITICAL)
    @Owner("alyona")
    @Description("test checks that Google opens right page")
    @Test(description = "test checks that Google opens right page ", alwaysRun = true)
    public void MyFirstSeleniumTest() {

        //Given
        googleSearchPage.open();
        //When
        googleSearchPage.search("automationpractice");
        //And
        googleResultPage.openUrlByName();
        Assert.assertEquals(URL_AUTOPRACTICE, driver.getCurrentUrl());
    }

    @AfterMethod(description = "take screenshot of test passed")
    public void screenShot() throws IOException {
        TakesScreenshot scr = ((TakesScreenshot) driver);
        File file1 = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file1, new File("src/test/testOutput/test1.PNG"));
        System.out.println("Screenshot of the test is taken");
    }
}
