package firstSuites;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import pages.browserFactory.DriverFactory;
import pages.browserFactory.DriverType;
import utils.EventHandler;
import utils.Waiters;

import java.util.concurrent.TimeUnit;

public abstract class BaseTest {
    private static EventFiringWebDriver eventDriver;

    @BeforeTest
    @Parameters("browser")
    public void setUp(String browser) {
        WebDriver driver = null;

        if (browser.equals("Firefox")) {
            driver = DriverFactory.getManager(DriverType.FIREFOX);
        } else if (browser.equals("Chrome")) {
            driver = DriverFactory.getManager(DriverType.CHROME);
        } else if (browser.equals("Safari")) {
            driver = DriverFactory.getManager(DriverType.SAFARI);
        } else {
            System.out.println("Invalid browser " + browser);
        }

        driver.manage().window().maximize();
        Waiters.implicitWait(driver, Waiters.TIME_TEN, TimeUnit.SECONDS);

        eventDriver = new EventFiringWebDriver(driver);

        EventHandler handler = new EventHandler();
        eventDriver.register(handler);
    }

    @AfterTest
    public static void tearDown() {
        eventDriver.close();
        System.out.println("All close up activities completed");
    }

    public static EventFiringWebDriver getDriver() {
        return eventDriver;
    }
}
