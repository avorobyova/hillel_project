package pages.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pages.BasePage;
import utils.Waiters;

public class GoogleResultPage extends BasePage {

    public static final String URL_AUTOPRACTICE = "http://automationpractice.com/index.php";

    public GoogleResultPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#search a")
    private WebElement clickBySearchLink;

    public void openUrlByName() {
        // WebElement clickBySearchLink = driver.findElement(By.cssSelector("#search a"))
        // xpath = "//a//cite[@class='iUh30'][contains(text(),'automationpractice.com')] | a//cite[@class='iUh30']"
        clickBySearchLink.click();
        Waiters.waitForUrl(driver, Waiters.TIME_FIVE, URL_AUTOPRACTICE);
    }

}
