package com.company;

public class ElectronicDevice {

    String brandName;
    String model;
    String color;
    double cost;
    double screenSize;
    int capacity;
    int powerBattery;
    int year;


    public ElectronicDevice(){

    }

    public ElectronicDevice (String brandName, String model){
        this.brandName = brandName;
        this.model = model;

    }

    public ElectronicDevice(String brandName, String model, String color){
        this.brandName = brandName;
        this.model = model;
        this.color = color;
    }

    public ElectronicDevice (String brandName, String model, double cost, double screenSize){
        this.brandName = brandName;
        this.model = model;
        this.cost = cost;
        this.screenSize = screenSize;
    }

    public ElectronicDevice (String brandName, String model, String color, double cost){
        this.brandName = brandName;
        this.model = model;
        this.color = color;
        this.cost = cost;
    }

    public ElectronicDevice(String brandName, String model, String color, double cost, double screenSize, int capacity, int powerBattery,int year){
        this.brandName = brandName;
        this.model = model;
        this.color = color;
        this.cost = cost;
        this.screenSize = screenSize;
        this.capacity = capacity;
        this.powerBattery = powerBattery;
        this.year = year;
    }

    public ElectronicDevice(String model, String color, double cost, int capacity, int year){
        this.model = model;
        this.color = color;
        this.cost = cost;
        this.capacity = capacity;
        this.year = year;
    }

    public ElectronicDevice(String model,String color, double cost, double screenSize, int capacity){
        this.model = model;
        this.color = color;
        this.cost = cost;
        this.screenSize = screenSize;
        this.capacity = capacity;
    }

    public ElectronicDevice(String model, String color, double cost,int year){
        this.model = model;
        this.color = color;
        this.cost = cost;
        this.year = year;
    }


}
