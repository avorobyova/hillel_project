package com.company.Exceptions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Prices {
    private Map<String, Double> prices;

    Prices() {
        this.prices = new HashMap<String, Double>();
        addItems();
    }

    private void addItems() {
        this.prices.put("chocolate", 10.0);
        this.prices.put("bread", 3.0);
        this.prices.put("milk", 15.0);
        this.prices.put("oranges", 20.0);
    }

    public void printAllPrices() {
        Set<String> keys = new HashSet<String>();
        keys.addAll(this.prices.keySet());
        keys.add("UNKNOWN ITEM");

        for (String itemName : keys) {
            try {
                double price = this.getPrice(itemName);
                System.out.println("The price for " + itemName + " is " + price);
            } catch (NoPriceException e) {
                System.out.println(e.toString());
            } finally {
                System.out.println("=================================");
            }
        }
    }

    public double getPrice(String name) throws NoPriceException {
        if (this.prices.containsKey(name)) {
            return this.prices.get(name);
        }
        throw new NoPriceException("The price for " + name + " havn't found.");
    }
}