package com.company.Exceptions;

import jdk.jshell.spi.ExecutionControl;

public class ThrowsException {

    static void fun() throws IllegalAccessException {
        System.out.println("Inside fun(). ");
        throw new IllegalAccessException("demo");
    }

    static void dontCallMe() throws ExecutionControl.NotImplementedException {
        throw new ExecutionControl.NotImplementedException("I'm not implemented yet.");
    }

    public static void main(String args[]) {
        try {
            dontCallMe();
            fun();
        } catch (IllegalAccessException e) {
            System.out.println("caught in main.");
        } catch (ExecutionControl.ExecutionControlException e) {
            e.printStackTrace();
        }
    }
}