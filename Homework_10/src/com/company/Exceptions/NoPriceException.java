package com.company.Exceptions;

public class NoPriceException extends Exception {
    NoPriceException(String errorMessage) {
        super(errorMessage);
    }
}
