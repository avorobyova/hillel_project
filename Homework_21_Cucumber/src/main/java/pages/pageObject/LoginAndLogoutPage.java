package pages.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import utils.Waiters;

public class LoginAndLogoutPage extends BasePage {

    public static final String URL_AUTOPRACTICE_LOGIN = "http://automationpractice.com/index.php?controller=authentication&back=my-account";

    public LoginAndLogoutPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@class='login']")
    private WebElement clickBySignIn;

    @FindBy(xpath = "//a[@class='logout']")
    private WebElement clickByLogout;

    public void openLoginPage() {
        clickBySignIn.click();
        Waiters.waitForUrl(driver, Waiters.TIME_FIVE, URL_AUTOPRACTICE_LOGIN);
    }

    public void clickLogOut(){
        clickByLogout.click();
    }
}
