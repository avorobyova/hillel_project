package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import pages.browserFactory.DriverFactory;
import pages.browserFactory.DriverType;
import utils.Waiters;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected static WebDriver driver;


    public static void setUp(String browser) {
        if (browser.equals("Firefox")) {
            driver = DriverFactory.getManager(DriverType.FIREFOX);
        } else if (browser.equals("Chrome")) {
            driver = DriverFactory.getManager(DriverType.CHROME);
        } else if (browser.equals("Safari")) {
            driver = DriverFactory.getManager(DriverType.SAFARI);
        } else {
            System.out.println("Invalid browser " + browser);
        }

        driver.manage().window().maximize();
        Waiters.implicitWait(driver, Waiters.TIME_TEN, TimeUnit.SECONDS);
    }

    @AfterEach
    public static void tearDown() {
        driver.close();
        System.out.println("All close up activities completed");
    }

    public static WebDriver getDriver() {
        return driver;
    }

}
