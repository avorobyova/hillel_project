package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.pageObject.LoginAndLogoutPage;

public class Login extends BaseTest {
    LoginAndLogoutPage loginAndLogoutPage;


    @Before
    public void init() {
        BaseTest.setUp("Chrome");
        loginAndLogoutPage = new LoginAndLogoutPage(driver);
    }

    @When("Click {string} button")
    public void clickButton(String btn) {
        System.out.println("Clicking " + btn + " button");
        loginAndLogoutPage.openLoginPage();
    }

//    @And("Click {string} button")
//    public void clickLogoutButton(String logoutbtn) {
//        System.out.println("Clicking " + logoutbtn + " button");
//        loginAndLogoutPage.clickLogOut();
//    }

    @After
    public static void tearDown() {
        driver.close();
        System.out.println("All close up activities completed");
    }

}
