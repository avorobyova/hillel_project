package steps;

import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import pages.browserFactory.DriverFactory;
import pages.browserFactory.DriverType;

public class Hooks {
    @Before
    public void openUrl() {
        WebDriver driver = BaseTest.getDriver();
        driver.get("http://automationpractice.com/index.php");
    }
}
