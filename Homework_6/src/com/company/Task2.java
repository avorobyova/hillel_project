package com.company;

import java.util.Arrays;
import java.util.Scanner;


public class Task2 {
    /*
    Для выполнения задания понадобиться Scanner s = new Scanner(System.in);

Необходимо ввести 10 значений с клавиатуры  и сохранить их в array, после этого вывести на экран.
     */

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);
        System.out.println("Введите число:");
        int arr[] = new int[10];
        int i = 0;

        do {
            if (src.hasNextInt()) {
                int n = src.nextInt();
                arr[i] = n;
                i++;
            } else {
                System.out.println("Вы ввели не число" + src.next());
            }
        } while (i < 10);


        System.out.println("Спасибо! Вы ввели числа, которые образуют массив " + Arrays.toString(arr));

    }

}
