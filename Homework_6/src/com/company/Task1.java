package com.company;

import java.util.Arrays;
import java.util.Random;

public class Task1 {

    public static void main(String[] args) {
        /* task 1
        1.Необходимо создать 2 массива из 10 случайных целых чисел в диапазоне от [0;10] каждый
        2. Необходимо вычислить  среднее арифметическое элементов каждого массива,
        если для первого массива среднеарифметическое значение оказалось больше, то выведите это через (sout),
        либо вывести что их среднеарифметические значения равны.*/
        Random r = new Random();
        int[] arr1 = new int[10];
        int[] arr2 = new int[10];

        for (int i = 0; i < 10; i++) {
            arr1[i] = r.nextInt(10);
            arr2[i] = r.nextInt(10);
        }
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));

        double averageArr1 = getAverage(arr1);
        double averageArr2 = getAverage(arr2);

        if (averageArr1 > averageArr2){
            System.out.println("Среднее арифметическое 1го массива больше чем для 2го -" +averageArr1);
        }
        else if (averageArr1 < averageArr2){
            System.out.println("Среднее арифметическое 2го массива больше чем для 1го -" +averageArr2);
        }
        else System.out.println("Среднее арифметическое 1го и 2го - равны");


    }

    public static double getAverage(int[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum / arr.length;

    }



}
