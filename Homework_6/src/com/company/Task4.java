package com.company;

import java.util.Arrays;

public class Task4 {
    /*
    Создайте массив размер которого = 10. Заполните его любимы числами.
Скопируйте все значения этого массива в другой массив в обратном порядке (10, 9, 8 …)
     */

    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] reversed = new int[10];


        for (int i = arr.length; i  > 0; i--) {
            reversed[i -1] = arr[arr.length - i];
        }

        System.out.println("Original array: " + Arrays.toString(arr));
        System.out.println("Reversed array:" + Arrays.toString(reversed));


    }
}