package com.company;

import java.util.Arrays;
import java.util.Random;

public class Task3 {

    /*
    Найти самое маленькое и самое большое значение в array и вывести их на экран. Возьмите размер array = 8.
     */

    public static void main(String[] args) {

        Random r = new Random();
        int[] arr1 = new int[8];

        for (int i = 0; i < 8; i++) {
            arr1[i] = r.nextInt(10);
        }

        System.out.println("Массив: " + Arrays.toString(arr1));
        System.out.println("максимальное значение в массиве: " + maximum(arr1));
        System.out.println("минимальное значение в массиве: " + minimum(arr1));


    }

    public static int maximum(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
                i++;
            }
        }
        return max;

    }

    public static int minimum(int[] arr) {
        int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
                i++;
            }
        }
        return min;

    }
}
