public class Methods {

    public static void main(String[] args) {
        int a = 13;
        int b = 10;
        int sum;

        sum = getSumm(a, b);
        System.out.println(sum);
        System.out.println(getSumm(45, 20));
        sum = getSumm(12, 48);
        // showSum(3,5,8);
        sayHello("Alyona");
        //showSumToPerson("Alyona",5,8,9);
        System.out.println("------------ Overload");
        int result;
        result = getSumm(3, 9);
        System.out.println(result);
        result = getSumm(6, 0, 2);
        System.out.println(result);
        int[] arr = {4, 9, 7};
        System.out.println("Sum of array elements is :" + getSumm(arr));


        devide(10, 3);
        try {
            devide(3.8, 80);
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }
    }

    static int getSumm(int x, int y) {
        return x + y;
    }

    //OverLoad method
    static int getSumm(int x, int y, int z) {
        return x + y + z;

    }

    //overLoad one more time
    static int getSumm(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    void showSum(int x, int y, int z) {
        int sum = x + y + z;
        System.out.println(sum);
    }

    static void sayHello(String name) {
        System.out.println();
        System.out.println("Hello, " + name);
        System.out.println("You're awesome!");
    }

    void showSumToPerson(String name, int a, int b, int c) {
        System.out.println("Start the program");
        sayHello(name);
        showSum(a, b, c);
        System.out.println("The end of progrmm");
    }

    public static void devide(int a, int b) {
        try {
            System.out.println("Result is: " + a / b);
        } catch (ArithmeticException e) {
            System.out.println("Problem!");
        } catch (RuntimeException e) {// может быть несколько блоков catch
            System.out.println("fvsdvsd");
        } finally {
            System.out.println("Finish"); //необязательный блок Finally который будет выполняться в любом случае
        }
    }

    public static void devide(double a, double b) throws ArithmeticException{
        if (b == 0) {
            throw new ArithmeticException("Cannot devide by zero!"); //вызывает исключение
        } else {
            System.out.println("Result is: " + a / b);
        }
    }
}
