import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

public class OnlineCourseOfBarancev {


    public static class MyListener extends AbstractWebDriverEventListener {

        @java.lang.Override
        public void beforeFindBy(By by, WebElement element, WebDriver driver) {
            System.out.println(" " + by);
        }


        public void afterFindBy(By by, WebElement element, WebDriver driver) {
            System.out.println(by + " is successfully found ");
        }


        public void onException(Throwable throwable, WebDriver driver) {
            System.out.println(throwable);
        }
    }

    public static ThreadLocal<EventFiringWebDriver> tlDriver = new ThreadLocal<>();
    private EventFiringWebDriver driver;
    private WebDriverWait wait;

    public boolean isElementPresent(WebDriver driver, By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (InvalidSelectorException ex) {
            throw ex;
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    public boolean areElementsPresent(WebDriver driver, By locator) {
        return driver.findElements(locator).size() > 0;
    }

    @Before
    public void start() {
        if (tlDriver.get() != null) {
            driver = tlDriver.get();
            wait = new WebDriverWait(driver, 10);
            return;
        }
        //ChromeDriverManager.getInstance().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-fullscreen");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
        //driver = new ChromeDriver(options);
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new EventFiringWebDriver(new ChromeDriver(caps));
        driver.register(new MyListener());

        tlDriver.set(driver);
        System.out.println(((HasCapabilities) driver).getCapabilities());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    driver.quit();
                }));
    }

    @FindBy(css = "input[q]")
    WebElement drag;

    @Test
    public void MyFirstTest() {
        driver.navigate().to("https://www.google.com/");
        wait = new WebDriverWait(driver, 10);
        driver.findElement(By.name("q")).sendKeys("webdriver" + Keys.ENTER + Keys.PAGE_DOWN);
        //driver.findElement(By.name("btnK")).click();
        wait.until(titleIs("webdriver - Пошук Google"));
        //driver.findElement(By.className("error"));
        //driver.findElement(By.cssSelector("div.page-header h2")).getCssValue("color");


//        new Actions(driver)
//                .moveToElement(drag).
//                keyDown(Keys.CONTROL).
//                clickAndHold().
//                moveToElement(drag).
//                release().
//                keyUp(Keys.CONTROL).
//                perform();
    }

    @Test
    public void MySecondTest() {
        driver.switchTo().window("open");
        driver.navigate().to("https://pagination.js.org/");
        WebElement demo1 = driver.findElement(By.cssSelector("#demo1"));
        List<WebElement> pagination = demo1.findElements(By.cssSelector(".paginationjs-pages ul li"));
        List<WebElement> items = demo1.findElements(By.cssSelector(".data-container ul li"));

        pagination.get(2).click();
        wait.until(ExpectedConditions.stalenessOf(items.get(0)));
        items = demo1.findElements(By.cssSelector(".data-container ul li"));
        Assert.assertTrue(items.get(0).getText().equals("11"));
//        driver.getWindowHandles();
//        driver.switchTo().window("open");
//        wait.until(visibilityOf(demo1));
//        wait.until(visibilityOfAllElements(items));
//        wait.until(invisibilityOfAllElements(items));
//        wait.until(visibilityOfElementLocated(By.className("bla bla bla")));
//        wait.until(alertIsPresent());
//        wait.until(numberOfWindowsToBe(2));
//        Alert alert = driver.switchTo().alert();
//        alert = wait.until(alertIsPresent());
//        alert.getText();
//        alert.sendKeys("qwerty");
//        alert.accept();
//        alert.dismiss();
//
//
//        driver.switchTo().frame(demo1);
//        driver.switchTo().defaultContent();
//        driver.switchTo().parentFrame();
//
//        driver.manage().window().maximize();
//        driver.manage().window().fullscreen();
//        driver.manage().window().getPosition();
//        driver.manage().window().setPosition(new Point(3,1));
//        driver.manage().window().getSize();


    }

//    @FindBy(css = "input[name=q]")
//    private WebElement searchField;
//    @Test
//    public void test2(){
//        searchField.sendKeys("webdriver");
//        searchField.submit();
//    }


    @After
    public void end() {
        driver.quit();
    }

}
