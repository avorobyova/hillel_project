public class LoopsAndConditions {

    public static void main (String [] args){

        int i =3;
        switch (i){
            case 1:
                System.out.println("this number is 1");
                break;
            case 2:
                System.out.println("this number is 2");
                break;
            case 3:
                System.out.println("this number is 3");
                break;
            default:
                System.out.println("I don't know the number");
        }

        for(i=0; i <10;i++){
            System.out.println(i);
        }

        System.out.println();
        for(i=10;i >0;i--){
            System.out.println(i);
        }

        int [] array = new int[10];// array definition
        System.out.println();
        for( i = 0;i<10;i++ ){
            array[i] =i*2;
        }
        System.out.println();
        for (i =0; i <10; i++){
            System.out.println("Element with index: "+i+" is "+array[i]);
        }

        for (int element: array){
            System.out.println(element);
        }

        int item=0;
        while (item<10){
            System.out.println("While: "+ item);
            item++;
        }

        i=0;
        boolean bool =true;
        while (bool){
            System.out.println("While: "+i);
            i++;
            if (i==5) bool=false;
        }


        i=1;
        do {
            System.out.println("Do: "+ i);
            i++;
        } while (i<5);


        int[] array1 ={2,3,3,3,4,6,4,2};
        int[] arr = new int[10];
        System.out.println(arr.equals(array1));
        int w =array.length;


    }

}
