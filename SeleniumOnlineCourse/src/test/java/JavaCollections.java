import com.sun.tools.javac.util.List;

import java.util.*;
import java.util.stream.Collectors;

public class JavaCollections {


    public static void main (String [] args) {

        ArrayList<Integer> list = new ArrayList<>(); // динамичный массив, размер которого можно изменять - добавлять и удалять элементы

        list.add(8);
        list.add(4);
        list.add(3);
        list.add(30);
        list.add(101);

        System.out.println("Second element of array:"+ list.get(1));
        int u = list.get(2);

        list.set(0,100);
        list.remove(2);
        System.out.println("Array size is "+ list.size());
        System.out.println();
        //list.clear();

        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(23);
        list2.add(100);

        list.addAll(3,list2); // добавляет список с указанного индекса
        list.addAll(list2); //добавили все элементы второго списка в конец первого списка

        System.out.println("Array list is " + list);

        list.removeAll(list2); // удалили все элементы второго списка из первого

        System.out.println("Array list is empty? " + list.isEmpty());// возвращает тру если список пустой

        System.out.println("Array list2 contains element 100? "+ list2.contains(100)); // проверяет что данный обьект - число 100 есть в списке

        System.out.println("Array list contains ALL list2? " + list.containsAll(list2)); //проверяем что все элементы второго списка есть в первом списке

        //итератор это интерфейс который позволяет работать с элементами списка или любой другой коллекции
        Iterator<Integer> iterator = list.iterator();
        iterator.next(); // указыает на следующий элемент в списке
        iterator.hasNext(); //узнать существует ли следующий элемент в списке
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println();
        //Set коллекции это множество уникальных обьектов/ элементов у которых нет индексов (даже если мы добавим такой же элмент)
        Set<Integer> set = new HashSet<>(); // самая популярная реализация
        set.add(9);
        set.add(10);
        set.add(11);
        set.add(11);
        set.add(11);
        set.add(11);

        System.out.println("Size of set: " + set.size());
        System.out.println(set);

        Iterator<Integer> iterator1 = set.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }

        set.remove(11);

        for(int i: set){
            System.out.println(i);
        }

        set.clear();
        System.out.println("size of set is "+set.size());
        System.out.println("The set contains 10 element? "+ set.contains(10));

        /*Map Collection это интерфейс который входит в CollectionsFramework НО НЕ входит в интерфейс Collection - отдельный интерфейс
        который позволяет хранить наборы данных, но в отличие от других коллекций данные тут хранится с виде пары key : value */
        Map<Integer, String> map = new HashMap<>();
        map.put(1,"white"); // add OR edit value of key
        map.put(2,"black");
        map.put(3,"Yellow");
        map.put(10,"Green");

        System.out.println("In map value of key 3 is " + map.get(3));
        System.out.println("In map value of key 10 is " + map.get(10));

        System.out.println(map.size());
        //map.clear();
        System.out.println(map.size());
        System.out.println(map.containsKey(2));// есть ли в мапе пару c указанным ключом

        System.out.println(map.containsValue("Green"));// есть ли в мапе пару c указанным значением

        Set<Integer> keys = map.keySet(); //этот метод позволяет получить сет множество всех ключей в мапе
        System.out.println(keys);
        Iterator<Integer> iterator3 = keys.iterator(); //создаем итератор из сета keys для цикла
        while (iterator3.hasNext()){
            System.out.println(map.get(iterator3.next()));
        }

        map.put(2,"Black");
        System.out.println(map.get(2));


        Map<String,String> map2 = new HashMap<>();
        map2.put("Vasya","Pupkin");
        map2.put("Alyona","one of a million");
        System.out.println(map2.getOrDefault("Alyona","one of a million"));


    }
}
