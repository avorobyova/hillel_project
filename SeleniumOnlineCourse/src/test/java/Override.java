public class Override extends Methods {
    Override meth = new Override();

    public Methods getMeth() {
        return meth;
    }

    void showSum(int x, int y, int z, double u){
        double sum = x+y+z+u;
        System.out.println(sum);
    }

    @java.lang.Override
    void showSum(int x, int y, int z) {
        super.showSum(x, y, z);
    }

    @java.lang.Override
    void showSumToPerson(String name, int a, int b, int c) {
        super.showSumToPerson(name, a, b, c);
    }
}
