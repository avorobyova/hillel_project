public class CarClass {
    int height;
    int width;
    int lenght;
    int weight;
    int maxWeight=2500;
    String color;
    int speed;
    int maxSpeed = 260;


    public double getVar2() {
        return var2;
    }

    public void setVar2(double var2) {
        this.var2 = var2;
    }

    private double var2 = 9.33;// для того чтобы заисползовать private переменную мы используем методы геттеры и сетерры


    /* модификатор static позволяет хранить метод или переменную в единственном экземпляре в классе
     и для того чтобы к ней обращаться вне класса мы должны использовать имя класса где этот метод/ переменная инициализирована

     чтобы получить значение переменной/ метода в другом классе не нужно создавать обьект класса а можно обратиться сразу через класс
     ТАКЖЕ изминение static переменных/ методов  производит изминение во всех обьектах данного класса
     */
    static int var =10;
    final static int var1 = 100; // модификатор final НЕ позволяет переопределять переменную или метод в других местах

    static void method(){
        System.out.println("Static method.");
    }




    /*
    Конструктор класса это метод, который вызывается при создании обьекта данного класса,
     название конструктора совпадает с названием класса
     */
    public  CarClass(){
        System.out.println("New car created!");

    }

    public  CarClass(String color){
        this.color = color;
    }
    public  CarClass(String color, int height, int width, int lenght){
        this.color = color;
        this.height = height;
        this.width = width;
        this.lenght = lenght;
    }

    //методы данного класса ниже
    public void addWeight(int weight){
        this.weight +=weight;
        System.out.println("New weight is "+ this.weight);
    }

    public void drive(int speed){
        if (weight<= maxWeight) {
            this.speed=speed;
            System.out.println("We are driving!");
        } else {
            System.out.println("Cannot drive!");
        }
    }

}
