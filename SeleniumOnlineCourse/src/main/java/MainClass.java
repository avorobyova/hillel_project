public class MainClass {


    /*
    Абстрактные классы это те классы, экземпляры которых мы не можем создовать
    НО мы можем их наследовать
    и абстрактные классы могут содержать ИЛИ не содержать абстрактные методы;
    абстрактные методы имеют сигнатуру, но не содержат тело метода - мы должны реализовать тело метода в наследуемом классе

    мы должны переопределить абстрактные методы в классе который наследуется от абстрактного  */


    /* ИНТЕРФЕЙС это полностью абстрактный класс в котором все методы являются публичными, абстрактными методами

    с помощью интерфейсом мы можем описать то как должен выглядить класс, какие методы он должен содержать, НО реализация
    данных методов перекладывается на самом класс который реализует наш интерфейс
     */
    public static void main(String[] args) {
        CarClass car1 = new CarClass();
        car1.weight=2000;
        car1.maxSpeed = 260;
        car1.color = "black";
        car1.lenght=5000;
        car1.width=2000;
        car1.height=2000;

        car1.addWeight(50);
        car1.drive(100);
        car1.addWeight(700);
        car1.drive(150);

        CarClass car2 = new CarClass();
        CarClass car3 = new CarClass();
        CarClass car4 = new CarClass("black");
        System.out.println("Car color is " + car4.color);
        CarClass car5 = new CarClass("green",1500,2000,5100);
        System.out.println(car5.color+" "+car5.height+" "+car5.width+" "+car5.lenght);


        System.out.println(CarClass.var);
        CarClass.var =100;
        System.out.println(car1.var);
        System.out.println(car2.var);
        System.out.println(car3.var);

        CarClass.method();

        /* Инкапсуляция - свойство системы, которое позволяет обьединять параметры и методы работы с ними в отдельный класы
        и скрывать их реалиацию от пользователя
         */


        /* Полиморфизм это способность обьектов с одинаковой спецификацией иметь различную реализацию
        - тесно связано с наследованием
        например переопределение методов (override) в дочерних классах (сигнатура остается прежняя) заключается в понятии
        полиморфизма
         */

        System.out.println(car1.getVar2());
        car1.setVar2(2.12);

        System.out.println(car1.getVar2());


    }
}
