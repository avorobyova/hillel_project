package com.company;

@FunctionalInterface
public interface Brushes {

    void washesBrush(double size); //абстрактный метод - мыть кисточки для макияжа

}

