package com.company;

public class EyeCosmetics extends Shadows implements Paint {

    private boolean isItGlitter; // переменная класса


    /* конструкторы для данного класса */
    public EyeCosmetics(String color, double size, String madeBy, String brand, boolean isItGlitter) {
        super(color, size, madeBy, madeBy);
        this.isItGlitter = isItGlitter;
    }

    public EyeCosmetics(String color, double weight, String madeBy, double size, boolean isItGlitter) {
        super(color, weight, madeBy, size);
        this.isItGlitter = isItGlitter;
    }

    /* создание Геттеров и Сеттеров для переменой private */
    public void setItGlitter(boolean isItGlitter) {
        this.isItGlitter = isItGlitter;
    }

    public boolean getItGlitter() {
        return isItGlitter;
    }

    /* унаследованый абстрактный метод из класса Shadows */
    public void mixColors() {
    }

    /* OVERLOAD */
    public void smell(EyeCosmetics cosmetics) {
        System.out.println("This " + cosmetics.getBrand() + "made in " + cosmetics.getMadeBy() + "smells great!");
    }

    @Override
    public boolean makeupRemoving(double water) {
        System.out.println("You also need sponges to remove makeup.");
        return false;
    }

    /* OVERLOAD */
    void paint(boolean isItGliter) {
        if (isItGliter == true) {
            System.out.println("Makeup is more bright using glitter");
        } else {
            System.out.println("Makeup is less bright without glitter ");
        }
    }

    class BrowsCosmetics { // inner class

        public void sharpenAPencil() {
        }
    }
}
