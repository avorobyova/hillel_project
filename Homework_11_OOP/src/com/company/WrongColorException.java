package com.company;

public class WrongColorException extends Exception {
    WrongColorException(String errorMessage) {
        super(errorMessage);
    }
}
