package com.company;

public class FaceCosmetics implements Paint, Brushes {

    private String typeOfSkin;
    private String color;
    private String brand;


    public void setTypeOfSkin(String typeOfSkin) {
        this.typeOfSkin = typeOfSkin;
    }

    public String getTypeOfSkin() {
        return typeOfSkin;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    /* constructor */
    public FaceCosmetics(String color, String brand, String typeOfSkin) {
        this.color = color;
        this.brand = brand;
        this.typeOfSkin = typeOfSkin;
    }


    @Override
    public void washesBrush(double size) {
        System.out.println("Makeup brushes should always be washed after use!");
    }

    @Override
    public boolean makeupRemoving(double water) {
        return true;
    }


    /* метод только для этого класса FaceCosmetics, где есть exception   */
    public void makeUp(String color) throws WrongColorException {
        if (this.color.equals(color) == false) {
            throw new WrongColorException("You apply a wrong color for make up!");
        }
        System.out.println("You look beautiful today!");
    }

    /* OVERLOAD */
    static void paint(String typeOfSkin) {
        switch (typeOfSkin) {
            case "dry":
                System.out.println("Your skin is so dry!");
                break;
            case "normal":
                System.out.println("Your skin is normal!");
                break;
            case "oily skin":
                System.out.println("Your skin is very oily!");
                break;
            default:
                System.out.println("You should choose cosmetics according to your skin type");
        }

    }
}
