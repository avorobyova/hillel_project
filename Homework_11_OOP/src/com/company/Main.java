package com.company;

public class Main {

    public static void main(String[] args) {

        System.out.println(Color.RED.getCode());        // #FF0000
        System.out.println(Color.GREEN.getCode());      // #00FF00

        Operation op = Operation.SUM;
        System.out.println(op.action(10, 4));   // 14
        op = Operation.MULTIPLY;
        System.out.println(op.action(6, 4));    // 24


        /* class instances */
        EyeCosmetics nyx = new EyeCosmetics("blue", 0.3, "USA", "nyx cosmetics", false);
        EyeCosmetics macCosmetics = new EyeCosmetics("gray", 0.6, "Germany", "macCosmetics", true);
        FaceCosmetics vichy = new FaceCosmetics("white", "vichy", "dry");
        Pencil kiko = new LipCosmetics("grey", "kiko", 1, 1.7);


        /* call the methods */
        nyx.smell();
        nyx.smell();
        nyx.makeupRemoving(4.6);
        nyx.paint(true);
        macCosmetics.makeupRemoving(3.5);
        macCosmetics.mixColors();
        macCosmetics.paint(false);
        macCosmetics.smell();
        vichy.makeupRemoving(2.9);
        vichy.setTypeOfSkin("dry");
        vichy.washesBrush(1.33);

        try {
            vichy.makeUp("blue");
        } catch (WrongColorException e) {
            System.out.println("You cannot make up via this color!");
        }

        ((LipCosmetics) kiko).makeupRemoving(2);
        kiko.sharpenAPencil();

    }

    /*  Color определяет приватное поле code для хранения кода цвета, а с помощью метода getCode оно возвращается.
    Через конструктор передается для него значение.
    Cоздать константы enum с помощью конструктора можно только внутри enum.
     */
    enum Color{
        RED("#FF0000"), BLUE("#0000FF"), GREEN("#00FF00");
        private String code;
        Color(String code){
            this.code = code;
        }
        public String getCode(){ return code;}
    }

    enum Operation{
        SUM{
            public int action(int x, int y){ return x + y;}
        },
        SUBTRACT{
            public int action(int x, int y){ return x - y;}
        },
        MULTIPLY{
            public int action(int x, int y){ return x * y;}
        };
        public abstract int action(int x, int y);
    }
}
