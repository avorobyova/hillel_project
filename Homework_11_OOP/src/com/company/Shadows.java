package com.company;

public abstract class Shadows {

    private String color;
    private String brand;
    private String typeOfSkin;
    private double size;
    private double weight;
    private String madeBy;

    public abstract void mixColors(); //абстрактный метод - "смешивать цвета"

    public void smell() {
        System.out.println("Smells great!");
    } //метод "Пахнуть", который не возаращает ничего

    public Shadows() {
        super();
    }

    /* несколько конструкторов для данного абстрактного класса */
    public Shadows(String color, String brand, String typeOfSkin, double size, double weight, String madeBy) {
        this.color = color;
        this.brand = brand;
        this.typeOfSkin = typeOfSkin;
        this.size = size;
        this.weight = weight;
        switch (this.madeBy = madeBy) {
        }
    }

    public Shadows(String color, double size, String madeBy, String brand) {
        this.color = color;
        this.size = size;
        this.madeBy = madeBy;
        this.brand = brand;
    }

    public Shadows(String color, double weight, String madeBy, double size) {
        this.color = color;
        this.weight = weight;
        this.madeBy = madeBy;
        this.size = size;
    }

    /* создание Геттеров и Сеттеров для переменых private */
    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setTypeOfSkin(String typeOfSkin) {
        this.typeOfSkin = typeOfSkin;
    }

    public String getTypeOfSkin() {
        return typeOfSkin;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setMadeBy(String madeBy) {
        this.madeBy = madeBy;
    }

    public String getMadeBy() {
        return madeBy;
    }


}
