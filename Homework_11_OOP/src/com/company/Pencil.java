package com.company;

public abstract class Pencil {

    private String color;
    private String brand;
    private long length;
    private double size;

    public abstract void sharpenAPencil(); //абстрактный метод - точить карандаш

    /*конструкторы для данного абстрактного класса */
    public Pencil(String color, String brand, long length, double size) {
        this.color = color;
        this.brand = brand;
        this.length = length;
        this.size = size;
    }

    public Pencil(String brand, double size) {
        this.brand = brand;
        this.size = size;
    }

    /* создание Геттеров и Сеттеров для переменых private */
    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getLength() {
        return length;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }
}
