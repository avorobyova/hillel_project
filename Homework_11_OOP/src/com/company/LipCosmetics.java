package com.company;

public class LipCosmetics extends Pencil implements Paint {

    public LipCosmetics(String color, String brand, long length, double size) {
        super(color, brand, length, size);
    }

    @Override
    public void sharpenAPencil() {
        System.out.println("To make-up was neat need to sharpen pencils");
    }

    @Override
    public boolean makeupRemoving(double water) {
        return true;
    }

}
