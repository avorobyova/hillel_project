package suit.test;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.pageObject.*;
import suit.BaseTest;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static pages.pageObject.GoogleSearchPage.URL_AUTOPRACTICE;

public class CheckWebsiteTest extends BaseTest {

    GoogleSearchPage googleSearchPage;
    GoogleResultPage googleResultPage;
    LoginAndLogoutPage loginAndLogoutPage;
    CheckColorOfFieldDuringLogin checkColorOfFieldDuringLogin;
    FilteringOfProducts filteringOfProducts;
    WebDriver driver;

    @BeforeTest
    public void startDriver() {
        driver = getDriver();
        googleSearchPage = new GoogleSearchPage(driver);
        googleResultPage = new GoogleResultPage(driver);
        loginAndLogoutPage = new LoginAndLogoutPage(driver);
        checkColorOfFieldDuringLogin = new CheckColorOfFieldDuringLogin(driver);
        filteringOfProducts = new FilteringOfProducts(driver);
    }

    @Test(description = "test checks that Google opens right page ", alwaysRun = true, priority = 1)
    public void CheckThatGoogleOpensRightPage() {
        //Given
        googleSearchPage.open();
        //When
        googleSearchPage.search("automationpractice");
        //And
        System.out.println("Page title is: " + driver.getTitle());
        googleResultPage.openUrlByName();
        assertEquals(URL_AUTOPRACTICE, driver.getCurrentUrl());
    }

    @Test(dataProvider = "dataForLogin", description = "check success login and logout on web site", priority = 2)
    public void checkLoginAndLogout(String email, String password) {
        loginAndLogoutPage.openLoginPage();
        //loginAndLogoutPage.loginByCredentials();
        driver.findElement(By.xpath("//input[@id='email']")).sendKeys(email);
        driver.findElement(By.xpath("//input[@id='passwd']")).sendKeys(password);
        driver.findElement(By.xpath("//p[@class='submit']//span[1]")).click();
        /*
		if (driver.findElement(By.id("profile-page")).isDisplayed()) {
			// Print a Log In message to the screen
			System.out.println("Login successfully.");
		}*/
        WebElement source = driver.findElement(By.cssSelector("nav > div:nth-child(1) > a > span"));
        // Assertion on correctness of user name
        Assert.assertEquals("Alyona Vorobiova", source.getText());
        loginAndLogoutPage.clickLogOut();

    }


    @Test(description = "if User entered incorrect value in 'email' field, this field should be RED color", priority = 4)
    public void checkRedColorOfEmailFieldDuringLogin() {
        checkColorOfFieldDuringLogin.openLoginPage();
        //Verify color of email field is Red, when User entered invalid value
        checkColorOfFieldDuringLogin.enterInvalidValue();

    }

    @Test(description = "if User entered correct value in 'email' field, this field should be GREEN color", priority = 5)
    public void checkGreenColorOfEmailFieldDuringLogin() {
        checkColorOfFieldDuringLogin.openLoginPage();
        //Verify color of email field is Green, when User entered valid value
        checkColorOfFieldDuringLogin.enterValidValue();

    }

    @Test(description = "check that title matches of the page to 'My store' ", priority = 3)
    public void testCaseVerifyTitleHomePage() {
        Assert.assertEquals("Login - My Store", driver.getTitle(), "Title of web page is not matching");
    }


    @Test(description = "check that menu of sections is displayed when User move mouse to element on web page ", priority = 6)
    public void moveMouseOnElement() {
        filteringOfProducts.moveMouseOnElement();
        Assert.assertTrue(driver.findElement(By.xpath("//div[@id='block_top_menu']//div[1]//img[1]")).isDisplayed(), "Sub menu isn't dispplayed");

        filteringOfProducts.openListOfProduct(); //open web-page Women clothes

    }

    @Test(dataProvider = "dataForFiltering", description = "filtering list of product women clothes", priority = 7)
    public void filteringOfProducts(String categories, String size, String color) {
        filteringOfProducts.openListOfProduct(); //open web-page Women clothes
        driver.findElement(By.cssSelector(categories)).click();
        driver.findElement(By.cssSelector(size)).click();
        driver.findElement(By.cssSelector(color)).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(categories)).isSelected(), "checkbox isn't selected");


    }


    @Test(enabled = false)
    public void skipMethod() {
        System.out.println("this method will be skipped from the test run using the attribute enabled=false");
    }

    @Test(priority = 8, invocationCount = 5, invocationTimeOut = 20)
    public void invocationcountShowCaseMethod() {
        System.out.println("this method will be executed by 5 times");
    }

    @AfterMethod(description = "take screenshot of test passed")
    public void screenShot() throws IOException {
        TakesScreenshot scr = ((TakesScreenshot) driver);
        File file1 = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file1, new File("src/test/testOutput/test1.PNG"));
        System.out.println("Screenshot of the test is taken");
    }
}
