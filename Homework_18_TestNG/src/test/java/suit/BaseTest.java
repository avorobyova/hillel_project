package suit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pages.browserFactory.DriverFactory;
import pages.browserFactory.DriverType;
import utils.Waiters;

import java.util.concurrent.TimeUnit;

public abstract class BaseTest {
    private static WebDriver driver;


    @DataProvider
    public static Object[][] dataForLogin() {
        return new Object[][]{
                new Object[]{"vorobbyova@gmail.com", "12345678"},
                new Object[]{"vorobbyova1@gmail.com", "01234567"},
                new Object[]{"vorobbyova2@gmail.com", "0123456"},
        };
    }

    @DataProvider
    public static Object[][] dataForFiltering() {
        return new Object[][]{
                new Object[]{"#layered_category_4", "#layered_id_attribute_group_1", "#layered_id_attribute_group_13"},
                new Object[]{"#layered_category_8", "#layered_id_attribute_group_2", "#layered_id_attribute_group_16"},
                new Object[]{"#layered_category_8", "#layered_id_attribute_group_3", "#layered_id_attribute_group_14"}
        };
    }

    @BeforeTest
    @Parameters("browser")
    public void setUp(String browser) {
        if (browser.equals("Firefox")) {
            driver = DriverFactory.getManager(DriverType.FIREFOX);
        } else if (browser.equals("Chrome")) {
            driver = DriverFactory.getManager(DriverType.CHROME);
        } else if (browser.equals("Safari")) {
            driver = DriverFactory.getManager(DriverType.SAFARI);
        } else {
            System.out.println("Invalid browser " + browser);
        }

        driver.manage().window().maximize();
        Waiters.implicitWait(driver, Waiters.TIME_TEN, TimeUnit.SECONDS);
    }

    @AfterTest
    public static void tearDown() {
        driver.close();
        System.out.println("All close up activities completed");
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
