package pages.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import utils.Waiters;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckColorOfFieldDuringLogin extends BasePage {

    public CheckColorOfFieldDuringLogin(WebDriver driver) {
        super(driver);
    }

    public static final String URL_AUTOPRACTICE_LOGIN = "http://automationpractice.com/index.php?controller=authentication&back=my-account";

    @FindBy(xpath = "//div/nav/div[1]/a")
    private WebElement clickBySignIn;
    //"//a[@class='login']"

    public void openLoginPage() {
        clickBySignIn.click();
        Waiters.waitForUrl(driver, Waiters.TIME_FIVE, URL_AUTOPRACTICE_LOGIN);
    }

    @FindBy (xpath = "//input[@id='email']")
    private WebElement emailField;

    public void enterInvalidValue(){
        emailField.sendKeys("111111");
        driver.findElement(By.xpath("//body")).click();
        assertEquals("rgba(255, 241, 242, 1)",
                emailField.getCssValue("background-color").toString());
        Actions builder = new Actions(driver);
        builder.doubleClick(emailField).build().perform();
        emailField.clear();
    }

    public void enterValidValue(){
        emailField.sendKeys("vorobbyova1@gmail.com");
        driver.findElement(By.xpath("//body")).click();
        assertEquals("rgba(221, 249, 225, 1)",
                emailField.getCssValue("background-color").toString());
        Actions builder = new Actions(driver);
        builder.doubleClick(emailField).build().perform();
    }
}
