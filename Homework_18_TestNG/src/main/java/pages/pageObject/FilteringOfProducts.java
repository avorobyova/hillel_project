package pages.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import utils.Waiters;

import java.util.concurrent.TimeUnit;

public class FilteringOfProducts extends BasePage {
    public FilteringOfProducts(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
    private WebElement womanIcon;

    public void moveMouseOnElement() {
        Actions builder = new Actions(driver);
        builder.moveToElement(womanIcon, 111, 59).build().perform();
        Waiters.implicitWait(driver, Waiters.TIME_FIVE, TimeUnit.SECONDS);
    }

    public void openListOfProduct() {
        womanIcon.click();
    }

    public void selectParametersForFiltering() {

    }

}
