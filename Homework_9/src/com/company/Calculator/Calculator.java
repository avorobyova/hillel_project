package com.company.Calculator;

public class Calculator {

    private double a;
    private double b;
    private double c;

    public Calculator() {

    }

    public double minus(double a, double b) {
        return a - b;
    }

    public double divide(double a, double b) {
        return a / b;
    }

    public double adding(double a, double b) {
        return a + b;
    }

    public double multiplication(double a, double b) {
        return a * b;
    }


    public double adding(double a, double b, double c) {
        return a + b + c;
    }

    public double multiplication(double a, double b, double c) {
        return a * b * c;
    }

    public double moduloDivision(double b, double c) {
        return b % c;
    }

    public void squared(double a) {
        a *= a;
    }

    public void cube(double b) {
        b = b * b * b;
    }

    public void squareRoot(double c) {
        double t;

        double squareRoot = c / 2;

        do {
            t = squareRoot;
            squareRoot = (t + (c / t)) / 2;
        } while ((t - squareRoot) != 0);
    }

    //Getters and Setters creating

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }


}
