package com.company;

public class Samsung extends Android_OS {
    private String country;

    public Samsung() {

    }

    public Samsung(String brandName, String model, String color, double cost) {
        super(brandName, model, color, cost);
    }

    public Samsung(String model, String color, double cost, double screenSize, int capacity) {
        super(model, color, cost, screenSize, capacity);
    }

    public Samsung(String model, String color, double cost, int capacity, int year) {
        super(model, color, cost, capacity, year);
    }

    static void printCounty(String country) {
        System.out.println("The country is " + country);
    }


    @Override
    public boolean isNew() {
        System.out.println("Samsung is much better than Meizu");
        return super.isNew();
    }

    @Override
    public boolean isOld() {
        System.out.printf("Old Samsung device is OK");
        return super.isOld();
    }
}
