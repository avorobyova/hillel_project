package com.company;

public class IOS extends ElectronicDevice {
    private static boolean canBePairWithWatch;
    private int levelOfPopularity;


    public IOS() {

    }

    public IOS(String brandName, String model) {
        super(brandName, model);
    }

    public IOS(String brandName, String model, String color, double cost) {
        super(brandName, model, color, cost);
    }

    public IOS(String brandName, String model, String color, double cost, double screenSize, int capacity, int powerBattery, int year) {
        super(brandName, model, color, cost, screenSize, capacity, powerBattery, year);
    }

    public IOS(String model, String color, double cost, int year) {
        super(model, color, cost, year);
    }

    static void isAbleToPairWithWatch() {
        if (canBePairWithWatch) System.out.println("this apple device can be paired with Apple watch");
    }

    //overloading
    public boolean isOld(String printMeIfDeviceIsOld) {
        if (super.isOld()) {
            System.out.println();
            return true;
        }
        return false;

    }

    @Override
    public boolean isNew() {
        return super.isNew();
    }
}
