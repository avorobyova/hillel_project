package com.company;

public class Meizu extends Android_OS {
    private String designedBy;
    protected int b;

    Meizu() {

    }

    public Meizu(String model, String color, double cost, int capacity, int year) {
        super(model, color, cost, capacity, year);
    }

    public Meizu(String brandName, String model, String color, double cost) {
        super(brandName, model, color, cost);
    }

    public Meizu(String model, String color, double cost, double screenSize, int capacity) {
        super(model, color, cost, screenSize, capacity);
    }

    @Override
    public boolean isOld() {
        System.out.println("Meizu is chinese device");
        return super.isOld();
    }

    //overloading
    public boolean isNew(int b) {
        return super.isNew();
    }
}
