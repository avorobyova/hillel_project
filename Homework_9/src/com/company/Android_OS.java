package com.company;

public class Android_OS extends ElectronicDevice {
    private double versionOfOS;

    public Android_OS() {

    }

    public Android_OS(String brandName, String model, String color, double cost) {
        super(brandName, model, color, cost);
    }

    public Android_OS(String model, String color, double cost, int capacity, int year) {
        super(model, color, cost, capacity, year);
    }

    public Android_OS(String model, String color, double cost, double screenSize, int capacity) {
        super(model, color, cost, screenSize, capacity);
    }

    public static void playGame() {
        System.out.println("this game is amazing!");
    }

    @Override
    public boolean isOld() {
        System.out.println("Device that based on Android OS");
        return super.isOld();
    }

    @Override
    public boolean isNew() {
        return super.isNew();
    }
}
