package com.company;

import java.sql.Time;
import java.util.Calendar;

public class ElectronicDevice {

    private String brandName = "Unknown brand";
    private String model = "Unknown model";
    private String color;
    private double cost;
    private double screenSize;
    private int capacity;
    private int powerBattery;
    private int year;


    //list of methods

    public ElectronicDevice() {}

    public ElectronicDevice(String brandName, String model) {
        this.setBrandName(brandName);
        this.setModel(model);

    }

    public ElectronicDevice(String brandName, String model, String color) {
        this.setBrandName(brandName);
        this.setModel(model);
        this.setColor(color);
    }

    public ElectronicDevice(String brandName, String model, double cost, double screenSize) {
        this.setBrandName(brandName);
        this.setModel(model);
        this.setCost(cost);
        this.setScreenSize(screenSize);
    }

    public ElectronicDevice(String brandName, String model, String color, double cost) {
        this.setBrandName(brandName);
        this.setModel(model);
        this.setColor(color);
        this.setCost(cost);
    }

    public ElectronicDevice(String brandName, String model, String color, double cost, double screenSize, int capacity, int powerBattery, int year) {
        this.setBrandName(brandName);
        this.setModel(model);
        this.setColor(color);
        this.setCost(cost);
        this.setScreenSize(screenSize);
        this.setCapacity(capacity);
        this.setPowerBattery(powerBattery);
        this.setYear(year);
    }

    public ElectronicDevice(String model, String color, double cost, int capacity, int year) {
        this.setModel(model);
        this.setColor(color);
        this.setCost(cost);
        this.setCapacity(capacity);
        this.setYear(year);
    }

    public ElectronicDevice(String model, String color, double cost, double screenSize, int capacity) {
        this.setModel(model);
        this.setColor(color);
        this.setCost(cost);
        this.setScreenSize(screenSize);
        this.setCapacity(capacity);
    }

    public ElectronicDevice(String model, String color, double cost, int year) {
        this.setModel(model);
        this.setColor(color);
        this.setCost(cost);
        this.setYear(year);
    }

    //Methods are displaying below
    static void playGame() {
        System.out.println("User can play game on divice");
    }

    static void listenMusic() {
        System.out.println("User can listen music via electronic device");
    }

    static void call() {
        System.out.println("User can call somebody using electronic device");
    }

    public boolean isOld() {
        return Calendar.getInstance().get(Calendar.YEAR) - year > 1;
    }

    public boolean isNew() {
        return !isOld();
    }


    //Getters and Setters creating
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(double screenSize) {
        this.screenSize = screenSize;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getPowerBattery() {
        return powerBattery;
    }

    public void setPowerBattery(int powerBattery) {
        this.powerBattery = powerBattery;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        str.append(getBrandName());
        str.append(':');
        str.append(getModel());
        str.append('-');
        str.append(getYear());
        str.append('\n');

        return str.toString();
    }

}
